As Of 3/22/2017

 - 1.7r-171 Fix spoof settings

 - 1.7r-173 Add option to completely disable stories features

 - 1.7r-174 Added ability to hide follow suggestions and badge

 - 1.7r-175 Added ability to see full followers number

 - 1.7r-176 Fixed Disabling stories feature causing DMs not to show

 - 1.7r-178 Fixed not being able to save Images and stories on Sideload
